using System;
using System.Diagnostics;
using Genealogy.Table;

namespace Genealogy
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            //int maxWidth = 0;
            //Console.WriteLine("Enter the max width (in cells) of the family tree.");
            //maxWidth = Convert.ToInt32(Console.ReadLine());

            string handle;
            Console.WriteLine("\nEnter the handle of the focal individual.");
            handle = Console.ReadLine();

            int generationsToAllow;
            Console.WriteLine("\nEnter the number of generations to allow.");
            generationsToAllow = Convert.ToInt32(Console.ReadLine());

            Xml xml = Xml.Load(@"genealogy.xml");
            Child focus = new Child(xml, handle, 1, generationsToAllow);

            Console.WriteLine();

            BottomUpTable bottomUpTable = new BottomUpTable(focus);
            string savedFile = bottomUpTable.Export(System.IO.Path.Combine("rendered", string.Concat(focus.Person.Handle, "_", generationsToAllow,
                "gen", (generationsToAllow != 1 ? "s" : ""), ".html")));
            //FamilyTable familyTable = new FamilyTable(maxWidth, focus, generationsToAllow);
            //string savedFile = familyTable.Export();

            ProcessStartInfo openStartInfo = new ProcessStartInfo(savedFile);
            openStartInfo.UseShellExecute = true;
            System.Diagnostics.Process.Start(openStartInfo);
        }

        public static void Output(Child child, int level)
        {
            string indent = new string(' ', level * 4);
            Console.Write(indent);
            Console.Write("- ");
            Console.WriteLine(child);
            if (child.Marriages != null)
            {
                for (int index = 0; index < child.Marriages.Length; index++)
                {
                    Console.Write(indent);
                    Console.Write(" M: ");
                    Marriage marriage = child.Marriages[index];
                    Console.Write(marriage.GetPartner(child.Person));
                    if (marriage.Children != null)
                    {
                        Console.Write(" (");
                        Console.Write(marriage.Children);
                        Console.WriteLine(")");
                        for (int childIndex = 0; childIndex < marriage.Children.Count; childIndex++)
                        {
                            Output(marriage.Children[childIndex], level + 1);
                        }
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                }
            }
        }
    }
}

