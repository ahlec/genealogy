﻿using System;
using System.Collections;
using System.Xml.Linq;

namespace Genealogy
{
    public sealed class Marriage : IAncenstor, IComparable<Marriage>
    {
        private static Hashtable STORED_MARRIAGES = new Hashtable();

        private Marriage(Xml xml, string handle, int generationsToAllow, int currentGeneration)
        {
            Handle = handle;
            XElement marriageNode = xml.SelectElement("//marriages/marriage[@id='" + handle + "']");
            Male = Person.Get(xml, marriageNode.Attribute("male").Value);
            Female = Person.Get(xml, marriageNode.Attribute("female").Value);
            IsMalesOnlyMarriage = ((double)xml.EvaluateXPath("count(//marriages/marriage[@male='" + Male.Handle + "'])") == 1);
            IsFemalesOnlyMarriage = ((double)xml.EvaluateXPath("count(//marriages/marriage[@female='" + Female.Handle + "'])") == 1);

            XElement childrenNode = marriageNode.Element("children");
            if (childrenNode != null)
            {
                if (generationsToAllow - 1 > 0)
                {
                    Children = new Children(xml, this, childrenNode, generationsToAllow - 1, currentGeneration + 1);
                    if (Children.Count > 0)
                    {
                        HaveChildren = true;
                    }
                    else
                    {
                        HaveChildren = false;
                        Children = null;
                    }
                }
                else
                {
                    Children = null;
                    HaveChildren = false;
                }
            }
            else
            {
                Children = null;
                HaveChildren = false;
            }
        }

        public static Marriage Get(Xml xml, string handle, int generationsToAllow, int currentGeneration)
        {
            if (string.IsNullOrWhiteSpace(handle))
            {
                throw new ArgumentNullException();
            }

            if (STORED_MARRIAGES.ContainsKey(handle))
            {
                return STORED_MARRIAGES[handle] as Marriage;
            }

            Marriage newlyLoaded = new Marriage(xml, handle, generationsToAllow, currentGeneration);
            STORED_MARRIAGES.Add(handle, newlyLoaded);
            return newlyLoaded;
        }

        public readonly string Handle;
        public readonly Person Male;
        public readonly Person Female;
        public readonly bool HaveChildren;
        public readonly Children Children;
        public readonly bool IsMalesOnlyMarriage;
        public readonly bool IsFemalesOnlyMarriage;

        public Person GetPartner(Person target)
        {
            if (target == null)
            {
                throw new ArgumentNullException();
            }

            if (Male.Equals(target))
            {
                return Female;
            }
            else if (Female.Equals(target))
            {
                return Male;
            }

            throw new ArgumentException();
        }

        public int GetDescendantsCount()
        {
            int count = 0;
            if (Children != null)
            {
                for (int index = 0; index < Children.Count; index++)
                {
                    count += Children[index].GetSelfAndDescendantsCount();
                }
            }
            return count;
        }
        public int GetSelfAndDescendantsCount()
        {
            int count = 2;
            if (Children != null)
            {
                for (int index = 0; index < Children.Count; index++)
                {
                    count += Children[index].GetSelfAndDescendantsCount();
                }
            }
            return count;
        }

        public int CompareTo(Marriage marriageB)
        {
            int marriageBChildren = (marriageB.Children != null ? marriageB.Children.Count : 0);
            int marriageAChildren = (Children != null ? Children.Count : 0);

            return marriageBChildren.CompareTo(marriageAChildren);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Marriage))
            {
                return false;
            }
            Marriage other = obj as Marriage;
            return (string.Equals(Handle, other.Handle));
        }
        public override int GetHashCode()
        {
            return Handle.GetHashCode();
        }
        public override string ToString()
        {
            return string.Concat("Marriage of ", Male, " to ", Female, " (", Handle, ")");
        }
    }
}
