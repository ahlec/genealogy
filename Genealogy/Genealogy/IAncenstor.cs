﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genealogy
{
    public interface IAncenstor
    {
        int GetDescendantsCount();
        int GetSelfAndDescendantsCount();
    }
}
