﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Genealogy
{
    public class BinarySearchTree<T> : IEnumerable<T> where T : IComparable<T>
    {
        private class Node
        {
            public Node(T item)
            {
                Value = item;
            }
            public T Value;
            public Node Left;
            public Node Right;
            public Node Parent;
        }
        private Node _root;

        public int Count { get; private set; }
        public T Min { get; private set; }
        public T Max { get; private set; }

        public void Add(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            if (_root == null)
            {
                _root = new Node(item);
                Min = item;
                Max = item;
                Count = 1;
                return;
            }

            Node current = _root;
            while (current != null)
            {
                int comparison = item.CompareTo(current.Value);
                if (comparison < 0)
                {
                    if (current.Left == null)
                    {
                        current.Left = new Node(item);
                        current.Left.Parent = current;
                        Count++;
                        if (item.CompareTo(Min) < 0)
                        {
                            Min = item;
                        }
                        return;
                    }
                    else
                    {
                        current = current.Left;
                    }
                }
                else if (comparison > 0)
                {
                    if (current.Right == null)
                    {
                        current.Right = new Node(item);
                        current.Right.Parent = current;
                        Count++;
                        if (item.CompareTo(Max) > 0)
                        {
                            Max = item;
                        }
                        return;
                    }
                    else
                    {
                        current = current.Right;
                    }
                }
                else
                {
                    ArgumentException alreadyExists = new ArgumentException("The supplied value already exists within this binary search tree.", "item");
                    throw alreadyExists;
                }
            }
        }
        public bool Contains(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            Node current = _root;
            while (current != null)
            {
                int comparison = item.CompareTo(current.Value);
                if (comparison == 0)
                {
                    return true;
                }

                if (comparison < 0)
                {
                    current = current.Left;
                }
                else
                {
                    current = current.Right;
                }
            }

            return false;
        }
        public void Remove(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            Node current = _root;
            while (current != null)
            {
                int comparison = item.CompareTo(current.Value);
                if (comparison == 0)
                {
                    if (current.Left == null && current.Right == null)
                    {
                        if (current.Parent == null) // root node when count = 1
                        {
                            _root = null;
                            Min = default(T);
                            Max = default(T);
                            Count = 0;
                            return;
                        }
                        else
                        {
                            int parentComparison = item.CompareTo(current.Parent.Value);
                            if (parentComparison < 0)
                            {
                                current.Parent.Left = null;
                            }
                            else
                            {
                                current.Parent.Right = null;
                            }
                            Count--;
                            break;
                        }
                    }
                    else if (current.Left != null ^ current.Right != null)
                    {
                        if (current.Parent == null)
                        {
                            _root = current.Left ?? current.Right;
                            _root.Parent = null;
                            Count--;
                            break;
                        }
                        else
                        {
                            int parentComparison = item.CompareTo(current.Parent.Value);
                            if (parentComparison < 0)
                            {
                                current.Parent.Left = current.Left ?? current.Right;
                                (current.Left ?? current.Right).Parent = current.Parent;
                            }
                            else
                            {
                                current.Parent.Right = current.Left ?? current.Right;
                                (current.Left ?? current.Right).Parent = current.Parent;
                            }
                            Count--;
                            break;
                        }
                    }
                    else // has both right and left nodes
                    {
                        Node currentSuccessor = current.Right;
                        while (currentSuccessor.Left != null)
                        {
                            currentSuccessor = currentSuccessor.Left;
                        }

                        if (currentSuccessor != current.Right)
                        {
                            if (currentSuccessor.Right != null)
                            {
                                currentSuccessor.Parent.Left = currentSuccessor.Right;
                                currentSuccessor.Right.Parent = currentSuccessor.Parent;
                            }
                            else
                            {
                                currentSuccessor.Parent.Left = null;
                            }
                        }
                        else
                        {
                            current.Right = current.Right.Right;
                            if (current.Right != null)
                            {
                                current.Right.Parent = current;
                            }
                        }
                        current.Value = currentSuccessor.Value;
                        Count--;
                        break;
                    }
                }
                else if (comparison < 0)
                {
                    current = current.Left;
                }
                else
                {
                    current = current.Right;
                }
            }

            if (item.Equals(Min))
            {
                Node currentMin = _root;
                while (currentMin.Left != null)
                {
                    currentMin = currentMin.Left;
                }
                Min = currentMin.Value;
            }
            if (item.Equals(Max))
            {
                Node currentMax = _root;
                while (currentMax.Right != null)
                {
                    currentMax = currentMax.Right;
                }
                Max = currentMax.Value;
            }
        }
        public T Get(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            Node current = _root;
            while (current != null)
            {
                int comparison = item.CompareTo(current.Value);
                if (comparison == 0)
                {
                    return current.Value;
                }
                else if (comparison < 0)
                {
                    current = current.Left;
                }
                else
                {
                    current = current.Right;
                }
            }

            ArgumentException itemNotFound = new ArgumentException("Input does not exist within this binary search tree.", "item");
            throw itemNotFound;
        }
        public void Clear()
        {
            Count = 0;
            Min = default(T);
            Max = default(T);
            _root = null;
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
        public IEnumerator<T> GetEnumerator()
        {
            return Enumerate(_root).GetEnumerator();
        }
        private IEnumerable<T> Enumerate(Node node)
        {
            if (node != null)
            {
                foreach (T left in Enumerate(node.Left))
                {
                    yield return left;
                }
                yield return node.Value;
                foreach (T right in Enumerate(node.Right))
                {
                    yield return right;
                }
            }
        }
    }
}
