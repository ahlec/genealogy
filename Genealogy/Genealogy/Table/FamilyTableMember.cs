﻿using System;

namespace Genealogy.Table
{
    public class FamilyTableMember
    {
        public FamilyTableMember(bool isChild, object data, bool marriageNodeRight, bool onlySpouse, bool hasChildren)
        {
            IsChild = isChild;
            MemberData = data;
            MarriageNodeIsRight = marriageNodeRight;
            IsOnlySpouse = onlySpouse;
            HasChildren = hasChildren;
        }
        public readonly object MemberData;
        public readonly bool IsChild;
        public bool MarriageNodeIsRight;
        public readonly bool IsOnlySpouse;
        public readonly bool HasChildren;
    }
}
