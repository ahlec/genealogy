﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Genealogy.Table
{
    public class BottomUpTable
    {
        private class Subtable
        {
            public Subtable(Person leader, Person parent, RelationType relation, int distanceFromRoot, int width, int height, bool touchesBottom)
            {
                Leader = leader;
                ResponsibleParent = parent;
                ParentRelation = relation;
                DistanceFromRoot = distanceFromRoot;
                Width = width;
                Height = height;
                Cells = new object[height, width];
                TouchesBottom = touchesBottom;
            }
            public readonly Person Leader;
            public readonly Person ResponsibleParent;
            public readonly int DistanceFromRoot;
            public readonly RelationType ParentRelation;
            public readonly int Width;
            public readonly int Height;
            public readonly object[,] Cells;
            public readonly bool TouchesBottom;
        }
        private class Line
        {
            public Line(bool up, bool down, bool left, bool right)
            {
                Up = up;
                Down = down;
                Left = left;
                Right = right;
            }
            public readonly bool Up;
            public readonly bool Down;
            public readonly bool Left;
            public readonly bool Right;

            public override string ToString()
            {
                return string.Join(" ", (Up ? "UP" : null), (Down ? "DOWN" : null), (Right ? "RIGHT" : null), (Left ? "LEFT" : null));
            }
        }
        private enum RelationType
        {
            Spouse,
            Child
        }

        private List<Subtable> _subtables = new List<Subtable>();

        public BottomUpTable(Child focus)
        {
            Focus = focus;
            PopulateDown(focus, null, RelationType.Child, 0);

            int distanceFromRootMax;
            while (_subtables.Count > 1)
            {
                distanceFromRootMax = _subtables.Max(subtable => subtable.DistanceFromRoot);
                Subtable focusSubtable = _subtables.Find(subtable => subtable.TouchesBottom && subtable.DistanceFromRoot == distanceFromRootMax);
                if (focusSubtable == null)
                {
                    throw new Exception("Oh dear");
                }

                Build(focusSubtable.ResponsibleParent, focusSubtable.ParentRelation);
            }
        }

        private void PopulateDown(Child current, Person parent, RelationType relation, int distanceFromRoot)
        {
            bool touchesBottom = true;
            for (int index = 0; index < current.Marriages.Length; index++)
            {
                bool spouseTouchesBottom = true;
                Children children = current.Marriages[index].Children;
                if (children != null)
                {
                    for (int childIndex = 0; childIndex < children.Count; childIndex++)
                    {
                        spouseTouchesBottom = false;
                        PopulateDown(children[childIndex], current.Marriages[index].GetPartner(current.Person), RelationType.Child,
                            distanceFromRoot + 2);
                    }
                }
                Subtable spouseSubtable = new Subtable(current.Marriages[index].GetPartner(current.Person), current.Person,
                    RelationType.Spouse, distanceFromRoot + 1, 1, 1, spouseTouchesBottom);
                spouseSubtable.Cells[0, 0] = current.Marriages[index].GetPartner(current.Person);
                _subtables.Add(spouseSubtable);
                touchesBottom = false;
            }

            Subtable meSubtable = new Subtable(current.Person, parent, relation, distanceFromRoot, 1, 1, touchesBottom);
            meSubtable.Cells[0, 0] = current.Person;
            _subtables.Add(meSubtable);
        }
        private void Build(Person parent, RelationType relationship)
        {
            List<Subtable> childSubtables = _subtables.FindAll(subtable => parent.Equals(subtable.ResponsibleParent));
            _subtables.RemoveAll(subtable => parent.Equals(subtable.ResponsibleParent));
            Subtable parentSubtable = _subtables.Find(subtable => parent.Equals(subtable.Leader));
            _subtables.Remove(parentSubtable);

            int workspaceWidth = childSubtables.Sum(subtable => subtable.Width) + childSubtables.Count + 1; // width of each table, plus
                                // the lines in between tables, plus the leader cell
            int workspaceHeight = childSubtables.Max(subtable => subtable.Height) + 3;
            object[,] workspace = new object[workspaceHeight, workspaceWidth];
            if (relationship == RelationType.Child)
            {
                int x = 0;
                int childIndex = 0;
                int minRootX = -1;
                int maxRootX = -1;
                foreach (Subtable child in childSubtables)
                {
                    Copy(child.Cells, ref workspace, x, 3);
                    bool hitRootChild = false;
                    bool isLastChild = (childIndex == childSubtables.Count - 1);
                    for (int lineX = 0; lineX < child.Width; lineX++)
                    {
                        if (!hitRootChild && workspace[3, lineX + x] is Person && workspace[3, lineX + x].Equals(child.Leader))
                        {
                            hitRootChild = true;
                        }

                        if (!hitRootChild && x == 0)
                        {
                            continue;
                        }

                        if (minRootX == -1)
                        {
                            minRootX = x + lineX;
                        }
                        maxRootX = x + lineX;
                        workspace[2, x + lineX] = new Line(false, child.Leader.Equals(workspace[3, lineX + x]), (x > 0 ||
                            !child.Leader.Equals(workspace[3, lineX + x])), (!isLastChild || !hitRootChild));

                        if (hitRootChild && isLastChild)
                        {
                            break;
                        }
                    }
                    x += child.Width;
                    if (!isLastChild)
                    {
                        workspace[2, x] = new Line(false, false, true, true);
                        maxRootX = x;
                        x++;
                    }
                    childIndex++;
                }

                x--;
                Line centerLine = workspace[2, (int)Math.Round(minRootX + (maxRootX - minRootX) / 2D)] as Line;
                workspace[2, (int)Math.Round(minRootX + (maxRootX - minRootX) / 2D)] = new Line(true, centerLine.Down, centerLine.Left, centerLine.Right);
                workspace[1, (int)Math.Round(minRootX + (maxRootX - minRootX) / 2D)] = new Line(true, true, false, false);
                workspace[0, (int)Math.Round(minRootX + (maxRootX - minRootX) / 2D)] = parent;
            }
            else
            {
                int x = 0;
                bool beganDrawingPlacements = false;
                bool placedParent = false;
                for (int index = 0; index < childSubtables.Count; index++)
                {
                    Copy(childSubtables[index].Cells, ref workspace, x, 0);
                    int width = childSubtables[index].Width;
                    bool hitRootChild = false;
                    for (int lineX = 0; lineX < width; lineX++)
                    {
                        if (!hitRootChild && childSubtables[index].Cells[0, lineX] is Person &&
                            childSubtables[index].Cells[0, lineX].Equals(childSubtables[index].Leader))
                        {
                            hitRootChild = true;
                            beganDrawingPlacements = true;

                            if (index == childSubtables.Count - 1 && placedParent)
                            {
                                break;
                            }
                            continue;
                        }

                        if (!hitRootChild && !beganDrawingPlacements)
                        {
                            continue;
                        }

                        workspace[0, x + lineX] = new Line(false, false, true, true);
                    }
                    x += childSubtables[index].Width;

                    if (index == childSubtables.Count - 1 && placedParent)
                    {
                        break;
                    }



                    workspace[0, x] = new Line(false, false, true, true);
                    x++;

                    if (!placedParent && (index == (int)Math.Round(childSubtables.Count / 2D) - 1 || index == childSubtables.Count - 1))
                    {
                        workspace[0, x] = parent;
                        x++;
                        if (index != childSubtables.Count - 1)
                        {
                            workspace[0, x] = new Line(false, false, true, true);
                            x++;
                        }
                        placedParent = true;
                    }
                }
            }

            int minX = -1;
            int minY = -1;
            int maxX = -1;
            int maxY = -1;
            for (int y = 0; y < workspaceHeight; y++)
            {
                for (int x = 0; x < workspaceWidth; x++)
                {
                    if (workspace[y, x] != null)
                    {
                        if (x < minX || minX == -1)
                        {
                            minX = x;
                        }
                        if (x > maxX || maxX == -1)
                        {
                            maxX = x;
                        }
                        if (y < minY || minY == -1)
                        {
                            minY = y;
                        }
                        if (y > maxY || maxY == -1)
                        {
                            maxY = y;
                        }
                    }
                }
            }

            if (minX < 0 || minY < 0 || maxX < minX || maxY < minY)
            {
                throw new Exception("Oh dear");
            }

            Subtable newSubtable = new Subtable(parent, parentSubtable.ResponsibleParent, parentSubtable.ParentRelation,
                parentSubtable.DistanceFromRoot, maxX - minX + 1, maxY - minY + 1, true);
            for (int y = minY; y <= maxY; y++)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    newSubtable.Cells[y - minY, x - minX] = workspace[y, x];
                }
            }
            _subtables.Add(newSubtable);
        }
        private void Copy(object[,] source, ref object[,] destination, int destinationX, int destinationY)
        {
            for (int y = 0; y < source.GetLength(0); y++)
            {
                for (int x = 0; x < source.GetLength(1); x++)
                {
                    destination[destinationY + y, destinationX + x] = source[y, x];
                }
            }
        }

        public readonly Child Focus;

        public string Export(string filename)
        {
            FileStream file = new FileStream(filename, FileMode.Create);
            StreamWriter writer = new StreamWriter(file);

            writer.WriteLine(@"<style>
td
{
  font-size:70%;
}
td.line
{
  height:40px;
  min-height:40px;
  min-width:40px;
  position:relative;
}
td.line img
{
  width:100%;
  height:100%;
}
td.person
{
  border:2px solid black;
}
td.person.focus
{
  background-color:skyblue;
}
</style>
<script>
function showMarriages()
{
    var marriageHandle = document.getElementById('marriage-find').value;
    var marriageCells = document.getElementsByClassName(marriageHandle + '-MARRIAGE');
    for (var index = 0; index < marriageCells.length; index++)
    {
        marriageCells[index].style.backgroundColor = 'pink';
    }
}
</script>");

            writer.WriteLine("<table>");

            Subtable table = _subtables[0];

            int y;
            int x;
            object cell;
            Person occupant;
            Line line;
            for (y = 0; y < table.Height; y++)
            {
                writer.WriteLine("  <tr>");
                for (x = 0; x < table.Width; x++)
                {
                    cell = table.Cells[y, x];
                    if (cell is Person)
                    {
                        occupant = cell as Person;
                        writer.Write("    <td class=\"person");
                        if (Focus.Person.Equals(occupant))
                        {
                            writer.Write(" focus");
                        }
                        writer.Write("\">");
                        writer.Write("<a href=\"http://genealogy.obdurodon.org/findPerson.php?person=");
                        writer.Write(occupant.Handle);
                        writer.Write("\" target=\"_blank\">");
                        writer.Write(occupant);
                        writer.Write("</a>");
                        writer.WriteLine("</td>");
                    }
                    else if (cell is Line)
                    {
                        line = cell as Line;
                        writer.Write("    <td class=\"line\"><img src=\"");
                        writer.Write(GetLineImage(line));
                        writer.Write("\" title=\"" + line.ToString() + "\" />");
                        writer.WriteLine("</td>");
                    }
                    else
                    {
                        writer.WriteLine("    <td></td>");
                    }
                }
                writer.WriteLine("</tr>");
            }
            writer.WriteLine("</table>");

            writer.Flush();
            file.Flush();
            writer.Close();

            return file.Name;
        }

        private string GetLineImage(Line line)
        {
            byte directionFlags = 0; // 0000 [up][down][left][right]
            if (line.Up)
            {
                directionFlags += 8;
            }
            if (line.Down)
            {
                directionFlags += 4;
            }
            if (line.Left)
            {
                directionFlags += 2;
            }
            if (line.Right)
            {
                directionFlags += 1;
            }

            switch (directionFlags)
            {
                case 15:
                    {
                        return "images/cross.png";
                    }
                case 14:
                    {
                        return "images/vertical-left.png";
                    }
                case 13:
                    {
                        return "images/vertical-right.png";
                    }
                case 12:
                    {
                        return "images/vertical.png";
                    }
                case 11:
                    {
                        return "images/up-left-right.png";
                    }
                case 10:
                    {
                        return "images/up-left.png";
                    }
                case 9:
                    {
                        return "images/up-right.png";
                    }
                case 7:
                    {
                        return "images/horizontal-down.png";
                    }
                case 6:
                    {
                        return "images/left-down.png";
                    }
                case 5:
                    {
                        return "images/right-down.png";
                    }
                case 3:
                    {
                        return "images/horizontal.png";
                    }
                case 2:
                    {
                        return "images/left.png";
                    }
                default:
                    {
                        return "";
                    }
            }
        }        

    }
}
