﻿using System;
using System.Collections.Generic;

namespace Genealogy.Table
{
    public class StaggerInfo : IComparable<StaggerInfo>
    {
        private static int ID = 0;

        public StaggerInfo(int descentX, int descentY, Marriage marriage, List<FamilyTableMember> members, int numberChildren)
        {
            UniqueID = ++ID;
            Marriage = marriage;
            DescentNodeX = descentX;
            DescentNodeY = descentY;
            Members = members;
            MembersCount = Members.Count;
            NumberChildren = numberChildren;
            CompareKey = MembersCount + (UniqueID / 10000f);
            Jumps = false;
        }
        public StaggerInfo(StaggerInfo originalInfo, bool jumps)
        {
            UniqueID = originalInfo.UniqueID;
            DescentNodeX = originalInfo.DescentNodeX;
            DescentNodeY = originalInfo.DescentNodeY;
            Marriage = originalInfo.Marriage;
            Members = originalInfo.Members;
            MembersCount = originalInfo.Members.Count;
            NumberChildren = originalInfo.NumberChildren;
            CompareKey = originalInfo.CompareKey;
            Jumps = jumps;
        }

        public readonly int DescentNodeX;
        public readonly int DescentNodeY;
        public readonly int MembersCount;
        public readonly int UniqueID;
        public readonly int NumberChildren;
        public readonly Marriage Marriage;
        public readonly List<FamilyTableMember> Members;
        public readonly bool Jumps;

        private float CompareKey;

        public int CompareTo(StaggerInfo other)
        {
            if (Jumps != other.Jumps)
            {
                return Jumps.CompareTo(other.Jumps);
            }
            return CompareKey.CompareTo(other.CompareKey);
        }

        public override string ToString()
        {
            return string.Concat("Stagger #", UniqueID, ": ", MembersCount, " members, ", NumberChildren, " children");
        }
    }
}
