﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Genealogy.Table
{
    public class FamilyTable
    {
        private class Location
        {
            public Location(int x, int y)
            {
                X = x;
                Y = y;
            }

            public readonly int X;
            public readonly int Y;
        }
        private class Line
        {
            public Line(bool up, bool down, bool left, bool right)
            {
                Up = up;
                Down = down;
                Left = left;
                Right = right;
            }
            public readonly bool Up;
            public readonly bool Down;
            public readonly bool Left;
            public readonly bool Right;
            public object DEBUG_DATA;

            public override string ToString()
            {
                return string.Join(" ", (Up ? "UP" : null), (Down ? "DOWN" : null), (Right ? "RIGHT" : null), (Left ? "LEFT" : null));
            }
        }
        private class MarriagePlaceholder
        {
            public MarriagePlaceholder(Marriage marriage)
            {
                Marriage = marriage;
            }
            public readonly Marriage Marriage;
        }
        private class PlacePersonsResults
        {
            public PlacePersonsResults(int minLineX, int maxLineX, int middleIndex, bool hasMiddle, int childrenLeft, int childrenRight)
            {
                MinLineX = minLineX;
                MaxLineX = maxLineX;
                MiddleIndex = middleIndex;
                HasMiddle = hasMiddle;
                ChildrenLeft = childrenLeft;
                ChildrenRight = childrenRight;
            }
            public readonly int MiddleIndex;
            public readonly int MinLineX;
            public readonly int MaxLineX;
            public readonly bool HasMiddle;
            public readonly int ChildrenLeft;
            public readonly int ChildrenRight;
        }
        private class RegionSpan
        {
            public RegionSpan(int x, int y, int totalWidth, int availableLeft, int availableRight, bool touchesLeft, bool touchesRight)
            {
                X = x;
                Y = y;
                TotalWidth = totalWidth;
                AvailableLeft = availableLeft;
                AvailableRight = availableRight;
                TouchesLeftWall = touchesLeft;
                TouchesRightWall = touchesRight;
            }
            public readonly int X;
            public readonly int Y;
            public readonly int TotalWidth;
            public readonly int AvailableLeft;
            public readonly int AvailableRight;
            public readonly bool TouchesLeftWall;
            public readonly bool TouchesRightWall;
        }
        private class StaggerDirection
        {
            public StaggerDirection(Location destination, StaggerDirection nextStep)
            {
                Destination = destination;
                NextStep = nextStep;
            }
            public readonly Location Destination;
            public readonly StaggerDirection NextStep;
        }
        private class DelayedMarriage : IComparable<DelayedMarriage>
        {
            public DelayedMarriage(Location location, Marriage marriage)
            {
                Marriage = marriage;
                Location = location;
            }
            public readonly Marriage Marriage;
            public readonly Location Location;
            
            public int CompareTo(DelayedMarriage other)
            {
                return other.Marriage.CompareTo(Marriage);
            }
        }

        private readonly int _maxWidth;
        private readonly Child _focus;
        private readonly int _maxGenerations;
        private List<FamilyTableCell[]> _cells = new List<FamilyTableCell[]>();
        private BinarySearchTree<StaggerInfo> _staggers = new BinarySearchTree<StaggerInfo>();
        private PriorityQueue<DelayedMarriage> _marriagesToPlace = new PriorityQueue<DelayedMarriage>();

        public FamilyTable(int maxWidth, Child focus, int maxGenerations)
        {
            _maxWidth = maxWidth;
            _focus = focus;
            _maxGenerations = maxGenerations;
            AddRow();
            _cells[0][(int)Math.Round(_maxWidth / 2D)] = new FamilyTableCell(0, (int)Math.Round(_maxWidth / 2D), focus.Person, null);
            PlaceSpouses(focus, new Location((int)Math.Round(_maxWidth / 2D), 0));
            ExpandPerson(focus);

            while (_marriagesToPlace.Count + (_staggers.Count > 0 && !_staggers.Min.Jumps ? _staggers.Count : 0) > 0)
            {
                while (_staggers.Count > 0 && !_staggers.Min.Jumps)
                {
                    StaggerInfo staggerInfo = _staggers.Min;
                    _staggers.Remove(staggerInfo);
                    Stagger(staggerInfo);
                }

                while (_marriagesToPlace.Count > 0)
                {
                    DelayedMarriage marriage = _marriagesToPlace.Pop();
                    ExpandMarriage(marriage.Location, marriage.Marriage);
                }
            }

            int minXUsed = 0;
            bool foundMinX = false;
            for (int x = 0; x < _maxWidth; x++)
            {
                for (int y = 0; y < _cells.Count; y++)
                {
                    if (_cells[y][x] != null)
                    {
                        foundMinX = true;
                        minXUsed = x;
                        break;
                    }
                }
                if (foundMinX)
                {
                    break;
                }
            }
            int maxXUsed = 0;
            bool foundMaxX = false;
            for (int x = _maxWidth - 1; x >= 0; x--)
            {
                for (int y = 0; y < _cells.Count; y++)
                {
                    if (_cells[y][x] != null)
                    {
                        foundMaxX = true;
                        maxXUsed = x;
                        break;
                    }
                }
                if (foundMaxX)
                {
                    break;
                }
            }

            while (_staggers.Count > 0)
            {
                StaggerInfo staggerInfo = _staggers.Min;
                _staggers.Remove(staggerInfo);

            }
        }

        private Location GetLocation(Person person)
        {
            for (int y = 0; y < _cells.Count; y++)
            {
                for (int x = 0; x < _maxWidth; x++)
                {
                    FamilyTableCell cellValue = _cells[y][x];
                    if (cellValue == null)
                    {
                        continue;
                    }

                    if (cellValue.Occupant is Person && cellValue.Occupant.Equals(person))
                    {
                        return new Location(x, y);
                    }
                    else if (cellValue.Occupant is Child && (cellValue.Occupant as Child).Person.Equals(person))
                    {
                        return new Location(x, y);
                    }
                }
            }

            return null;
        }
        private void AddRow()
        {
            _cells.Add(new FamilyTableCell[_maxWidth]);
        }

        private void PlaceSpouses(Child person, Location personLocation)
        {
            int leftStart = (int)Math.Round(person.Marriages.Length / 2D) - (1 - (person.Marriages.Length % 2));
            int iteration = 0;
            for (int left = leftStart; left >= 0; left--)
            {
                _cells[personLocation.Y][personLocation.X + (left - leftStart - 1) * 2 + 1] =
                    new FamilyTableCell(personLocation.X + (left - leftStart - 1) * 2 + 1, personLocation.Y, 
                        new Line(false, (person.Marriages.Length == 1), true, true), null);
                person.Marriages[left].GetPartner(person.Person).DEBUG_DATA = string.Concat("left", iteration++);
                _cells[personLocation.Y][personLocation.X + (left - leftStart - 1) * 2] =
                    new FamilyTableCell(personLocation.X + (left - leftStart - 1) * 2, personLocation.Y,
                        person.Marriages[left].GetPartner(person.Person), null);
            }
            iteration = 0;
            int rightStart = leftStart + 1;
            for (int right = rightStart; right < person.Marriages.Length; right++)
            {
                _cells[personLocation.Y][personLocation.X + (right - rightStart) * 2 + 1] = 
                    new FamilyTableCell(personLocation.X + (right - rightStart) * 2 + 1, personLocation.Y,
                        new Line(false, false, true, true), null);
                person.Marriages[right].GetPartner(person.Person).DEBUG_DATA = string.Concat("right", iteration++);
                _cells[personLocation.Y][personLocation.X + (right - rightStart + 1) * 2] =
                    new FamilyTableCell(personLocation.X + (right - rightStart + 1) * 2, personLocation.Y,
                        person.Marriages[right].GetPartner(person.Person), null);
            }
        }

        private void ExpandPerson(Child person)
        {
            if (person.Marriages == null)
            {
                return;
            }

            Location location = GetLocation(person.Person);


            if (location == null)
            {
                return;
            }


            //PlaceSpouses(person, location);
            

            PriorityQueue<Marriage> marriagePriorityQueue = new PriorityQueue<Marriage>();
            for (int index = 0; index < person.Marriages.Length; index++)
            {
                marriagePriorityQueue.Add(person.Marriages[index]);
            }

            int marriageNum = -1;
            while (marriagePriorityQueue.Count > 0)
            {
                marriageNum++;
                Marriage marriage = marriagePriorityQueue.Pop();
                if (!marriage.HaveChildren || marriage.Children == null)
                {
                    continue;
                }

                Location parentLocation = GetLocation(marriage.GetPartner(person.Person));
                if (person.Marriages.Length == 1)
                {
                    Location parentBLocation = GetLocation(person.Person);
                    parentLocation = new Location((int)Math.Floor((parentBLocation.X + parentLocation.X) / 2D), parentLocation.Y);
                }

                if (parentLocation == null)
                {
                    continue;
                }

                ExpandMarriage(parentLocation, marriage);
            }

            for (int marriageIndex = 0; marriageIndex < person.Marriages.Length; marriageIndex++)
            {
                Marriage marriage = person.Marriages[marriageIndex];
                if (!marriage.HaveChildren || marriage.Children == null)
                {
                    continue;
                }
                for (int index = 0; index < marriage.Children.Count; index++)
                {
                    ExpandPerson(marriage.Children[index]);
                }
            }
        }
        private void ExpandMarriage(Location parentLocation, Marriage marriage)
        {
            while (_cells.Count <= parentLocation.Y + 3)
            {
                AddRow();
            }


            int availableSpaceLeft = -1;
            if (_cells[parentLocation.Y + 3][parentLocation.X] != null &&
                _cells[parentLocation.Y + 3][parentLocation.X].Occupant is MarriagePlaceholder)
            {
                _cells[parentLocation.Y + 3][parentLocation.X] = null;
            }
            bool hitLeftWall = false;
            for (int cell = parentLocation.X; cell >= 0; cell--)
            {
                if (_cells[parentLocation.Y + 3][cell] != null)
                {
                    break;
                }
                availableSpaceLeft++;
                if (cell == 0)
                {
                    hitLeftWall = true;
                }
            }
            int beginOffset = 0;
            bool availableSearchRightBegun = false;
            int availableSpaceRight = -1;
            bool hitRightWall = false;
            for (int cell = parentLocation.X; cell < _maxWidth; cell++)
            {
                if (!availableSearchRightBegun)
                {
                    if (_cells[parentLocation.Y + 2][cell] != null)
                    {
                        break;
                    }
                }
                if (_cells[parentLocation.Y + 3][cell] != null)
                {
                    if (!availableSearchRightBegun)
                    {
                        beginOffset++;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                if (availableSearchRightBegun)
                {
                    availableSpaceRight++;
                }
                else
                {
                    availableSearchRightBegun = true;
                    availableSpaceRight++;
                }

                if (cell == _maxWidth - 1)
                {
                    hitRightWall = true;
                }
            }

            parentLocation = new Location(parentLocation.X + beginOffset, parentLocation.Y);

            List<FamilyTableMember> members = GetFamilyTableMembers(marriage);
            RegionSpan span = new RegionSpan(parentLocation.X, parentLocation.Y, 1 + availableSpaceLeft + availableSpaceRight,
                availableSpaceLeft, availableSpaceRight, hitLeftWall, hitRightWall);
            PlacePersonsResults results = PlacePersons(marriage, members, marriage.Children.Count, new Location(parentLocation.X, parentLocation.Y + 3),
                span, beginOffset, true);// availableSpaceLeft, availableSpaceRight, beginOffset, true);

            if (results != null)
            {
                _cells[parentLocation.Y + 1][parentLocation.X - beginOffset] = new FamilyTableCell(parentLocation.X - beginOffset,
                    parentLocation.Y + 1, new Line(true, true, false, false), marriage);

                for (int fillerLines = 1; fillerLines <= beginOffset; fillerLines++)
                {
                    _cells[parentLocation.Y + 2][parentLocation.X - fillerLines] = new FamilyTableCell(parentLocation.X - fillerLines,
                        parentLocation.Y + 2, new Line((fillerLines == beginOffset), false,
                        (fillerLines != beginOffset), true), marriage);
                }
                _cells[parentLocation.Y + 2][parentLocation.X] = new FamilyTableCell(parentLocation.X, parentLocation.Y + 2,
                    new Line((beginOffset == 0), (results.HasMiddle &&
                    (results.MiddleIndex >= 0 && members[results.MiddleIndex].IsChild)),
                    (results.ChildrenLeft > 0 || beginOffset > 0), (results.ChildrenRight > 0)), marriage);


                if (results.MinLineX > parentLocation.X)
                {
                    for (int x = parentLocation.X + 1; x < results.MinLineX; x++)
                    {
                        _cells[parentLocation.Y + 2][x] = new FamilyTableCell(x, parentLocation.Y + 2, new Line(false, false, true, true),
                            marriage);
                    }
                }
                else if (results.MaxLineX < parentLocation.X)
                {
                    for (int x = parentLocation.X - 1; x > results.MaxLineX; x--)
                    {
                        _cells[parentLocation.Y + 2][x] = new FamilyTableCell(x, parentLocation.Y + 2, new Line(false, false, true, true),
                            marriage);
                    }
                }
            }
        }

        private static int PLACE = 0;


        private PlacePersonsResults PlacePersons(Marriage parentMarriage, List<FamilyTableMember> members, int childrenCount, Location centerLocation, 
            RegionSpan regionSpan, int beginOffset, bool allowStaggering)
        {
            int middleIndex = (int)Math.Floor(members.Count / 2D);
            bool hasMiddle = (childrenCount % 2 == 1);
            bool staggering = false;

            int availableSpaceLeft = regionSpan.AvailableLeft - (regionSpan.TouchesLeftWall ? 0 : 1);
            int availableSpaceRight = regionSpan.AvailableRight - (regionSpan.TouchesRightWall ? 0 : 1);

            //if (regionSpan.TouchesLeftWall != regionSpan.TouchesRightWall)
           // {
           //     hasMiddle = !hasMiddle;
           // }

            if (members.Count * 2 - (hasMiddle ? 1 : 0) > availableSpaceLeft + availableSpaceRight)
            {
                if (allowStaggering)
                {
                    staggering = true;
                    _cells[centerLocation.Y][centerLocation.X] = new FamilyTableCell(centerLocation.X, centerLocation.Y, 
                        "STAGGER (Need " + (members.Count * 2 - (hasMiddle ? 1 : 0)) + ", Have " +
                            (availableSpaceLeft + availableSpaceRight) + ")", parentMarriage);
                }
                else
                {
                    throw new Exception();
                }
            }

            /*int leftBorderX = centerLocation.X - middleIndex * 2 + (beginOffset == 0 ? (hasMiddle ? 0 : 1) : -1) - 2;
            if (_cells[centerLocation.Y][leftBorderX] != null && !(_cells[centerLocation.Y][leftBorderX].Occupant is Line))
            {
                middleIndex--;
                hasMiddle = !hasMiddle;
            }*/
            int rightBorderX = centerLocation.X - middleIndex * 2 + members.Count * 2 + (beginOffset == 0 ? (hasMiddle ? 0 : 1) : -1) + 1;
            if (_cells[centerLocation.Y][rightBorderX] != null && !(_cells[centerLocation.Y][rightBorderX].Occupant is Line ||
                _cells[centerLocation.Y][rightBorderX].Occupant is StaggerPlaceholder) && middleIndex < members.Count - 1)
            {
                middleIndex++;
                hasMiddle = !hasMiddle;
            }

            int staggerTESTCOUNT = 0;

            bool wentLeft = false;
            bool wentRight = false;

            // See if we can adjust membersLeft or membersRight to prevent a need for staggering
            if (!staggering && middleIndex * 2 > availableSpaceLeft)
            {
                if (availableSpaceRight - members.Count * 2 + (middleIndex * 2 - availableSpaceLeft) >= 0)
                {
                    int amountToShift = ((middleIndex + (hasMiddle ? 1 : 0)) * 2 - availableSpaceLeft) / 2;
                    middleIndex -= amountToShift;
                    staggering = false;
                    wentRight = true;
                }
                else
                {
                    staggerTESTCOUNT++;
                    //_cells[parentLocation.Y + 3][parentLocation.X] = "STAGGER (LEFT SHIFT FAIL)";
                    //staggering = true;
                }
            }
            if (!staggering && (members.Count - middleIndex) * 2 > availableSpaceRight)
            {
                if (availableSpaceLeft - middleIndex * 2 - ((members.Count - middleIndex) * 2 - availableSpaceRight) >= 0)
                {
                    int amountToShift = ((members.Count - middleIndex - (hasMiddle ? 1 : 0)) * 2 - availableSpaceRight) / 2;
                    middleIndex += amountToShift;
                    staggering = false;
                    wentLeft = true;
                }
                else
                {
                    staggerTESTCOUNT++;
                    //_cells[parentLocation.Y + 3][parentLocation.X] = "STAGGER (RIGHT SHIFT FAIL)";
                    //staggering = true;
                }
            }

            if (staggerTESTCOUNT == 2)
            {
                throw new Exception();
            }

            if (staggering)
            {
                if (allowStaggering)
                {
                    StaggerInfo staggerInfo = new StaggerInfo(centerLocation.X, centerLocation.Y, parentMarriage, members, childrenCount);
                    _staggers.Add(staggerInfo);
                    _cells[centerLocation.Y - 1][centerLocation.X] = new FamilyTableCell(centerLocation.X, centerLocation.Y - 1,
                        new Line(true, true, false, false), parentMarriage);
                    _cells[centerLocation.Y - 2][centerLocation.X] = new FamilyTableCell(centerLocation.X, centerLocation.Y - 2,
                        new Line(true, true, false, false), parentMarriage);
                    _cells[centerLocation.Y][centerLocation.X] = new FamilyTableCell(centerLocation.X, centerLocation.Y,
                        new StaggerPlaceholder(staggerInfo), parentMarriage);
                    return null;
                }
                else
                {
                    throw new Exception();
                }
            }


            // originally ChildrenCount == 1 here
            if (childrenCount == 1)
            {
                FamilyTableMember soleChildMember = members.Find(member => member.MemberData is Child);
                int childIndex = members.IndexOf(soleChildMember);
                while (childIndex < middleIndex)
                {
                    members[childIndex] = members[childIndex + 1];
                    members[childIndex].MarriageNodeIsRight = true;
                    childIndex++;
                }
                members[childIndex] = soleChildMember;
            }

            int childrenLeft = 0;
            int totalChildren = 0;
            int childrenRight = 0;
            for (int index = 0; index < members.Count; index++)
            {
                if (!members[index].IsChild)
                {
                    continue;
                }
                totalChildren++;
                if (index < middleIndex)
                {
                    childrenLeft++;
                }
                else if (hasMiddle && index > middleIndex || !hasMiddle && index >= middleIndex)
                {
                    childrenRight++;
                }
            }


            ++PLACE;


            int childrenCounted = 0;
            bool foundFirstChild = false;
            int minLineX = 0;
            int maxLineX = 0;
            bool foundMinLine = false;
            for (int index = 0; index < members.Count; index++)
            {
                FamilyTableMember member = members[index];
                int x = centerLocation.X - middleIndex * 2 + index * 2 + (beginOffset == 0 ? (hasMiddle ? 0 : 1) : -1);

                if (member.IsChild)
                {
                    childrenCounted++;
                    foundFirstChild = true;
                }

                if (hasMiddle && index == middleIndex && !foundMinLine)
                {
                    minLineX = x;
                    maxLineX = x;
                    foundMinLine = true;
                }

                if ((!hasMiddle || hasMiddle && index != middleIndex) && (childrenCounted < totalChildren || member.IsChild) && foundFirstChild)
                {
                    _cells[centerLocation.Y - 1][x] = new FamilyTableCell(x, centerLocation.Y - 1,
                        new Line(false, member.IsChild, (member.IsChild && childrenCounted > 1 ||
                        !member.IsChild && index > 0 || index >= middleIndex),
                        (childrenCounted < totalChildren || member.IsChild && index < middleIndex)), parentMarriage);
                    if (!foundMinLine)
                    {
                        minLineX = x;
                        foundMinLine = true;
                    }
                    maxLineX = x;
                }
                if (index < members.Count - 1 && x + 1 != centerLocation.X && childrenCounted < totalChildren && foundFirstChild)
                {
                    _cells[centerLocation.Y - 1][x + 1] = new FamilyTableCell(x + 1, centerLocation.Y - 1, new Line(false, false, true, true),
                        parentMarriage);
                    maxLineX = x + 1;
                }

                //if (index == middleIndex)
               // {
               //     ((member.IsChild ? ((Child)member.MemberData).Person : member.MemberData) as Person).DEBUG_DATA = "Placement #" + PLACE + "<br />Left: " + availableSpaceLeft + " (" + regionSpan.TouchesLeftWall +
               // ")<br />Right: " + availableSpaceRight + " (" + regionSpan.TouchesRightWall + ")<br />Has Middle: " + hasMiddle + "<br />MidInd: " +
               // middleIndex + "<br />Went Right: " + wentRight + "<br />Went Left: " + wentLeft;
                //}
                /*if (x == centerLocation.X)
                {
                    ((member.IsChild ? ((Child)member.MemberData).Person : member.MemberData) as Person).DEBUG_DATA =
                        "CENTERLOCATION + <br />MI: " + middleIndex + "<br />PLACEMENT: " + PLACE;
                }*/
                _cells[centerLocation.Y][x] = new FamilyTableCell(x, centerLocation.Y, member.MemberData, parentMarriage);// (member.IsChild ? ((Child)member.MemberData).Person : member.MemberData);

                if (!member.IsChild)
                {
                    _cells[centerLocation.Y][(member.MarriageNodeIsRight ? x + 1 : x - 1)] = new FamilyTableCell((member.MarriageNodeIsRight ?
                        x + 1 : x - 1), centerLocation.Y, new Line(false,
                        (member.IsOnlySpouse && member.HasChildren), true, true), parentMarriage);
                    if (member.HasChildren)
                    {
                        while (_cells.Count <= centerLocation.Y + 3)
                        {
                            AddRow();
                        }
                        if (_cells[centerLocation.Y + 3][(member.MarriageNodeIsRight ? x + 1 : x - 1)] == null)
                        {
                            _cells[centerLocation.Y + 3][(member.MarriageNodeIsRight ? x + 1 : x - 1)] =
                                new FamilyTableCell((member.MarriageNodeIsRight ? x + 1 : x - 1), centerLocation.Y + 3, new MarriagePlaceholder(null),
                                    parentMarriage);
                        }
                        else
                        {
                            RemoveMarriage(parentMarriage);
                            Location initialParentLocation = GetLocation(parentMarriage.Female);
                            bool onlyMarriage = false;
                            if (parentMarriage.Male.IsBloodDescendentOf(_focus.Person))
                            {
                                if (parentMarriage.IsMalesOnlyMarriage)
                                {
                                    onlyMarriage = true;
                                }
                            }
                            else
                            {
                                if (parentMarriage.IsFemalesOnlyMarriage)
                                {
                                    onlyMarriage = true;
                                }
                            }

                            if (onlyMarriage)
                            {
                                initialParentLocation = new Location((int)Math.Round((initialParentLocation.X + GetLocation(parentMarriage.Male).X) / 2D),
                                    initialParentLocation.Y);
                            }

                            //initialParentLocation = RemoveMarriage(_cells[initialParentLocation.Y - 1][initialParentLocation.X].ParentMarriage);
                            StaggerInfo restaggerInfo = new StaggerInfo(new StaggerInfo(initialParentLocation.X, initialParentLocation.Y + 3, parentMarriage,
                                GetFamilyTableMembers(parentMarriage), (parentMarriage.Children != null ? parentMarriage.Children.Count : 0)), true);
                            _staggers.Add(restaggerInfo);
                            _cells[initialParentLocation.Y + 3][initialParentLocation.X] = new FamilyTableCell(initialParentLocation.X,
                                initialParentLocation.Y + 3, new StaggerPlaceholder(restaggerInfo), parentMarriage);
                            _cells[initialParentLocation.Y + 2][initialParentLocation.X] = new FamilyTableCell(initialParentLocation.X,
                                initialParentLocation.Y + 2, new Line(true, true, false, false), parentMarriage);
                            _cells[initialParentLocation.Y + 1][initialParentLocation.X] = new FamilyTableCell(initialParentLocation.X,
                                initialParentLocation.Y + 1, new Line(true, true, false, false), parentMarriage);

                            return null;
                        }
                    }
                }
            }

            return new PlacePersonsResults(minLineX, maxLineX, middleIndex, hasMiddle, childrenLeft, childrenRight);
        }

        private void Stagger(StaggerInfo staggerInfo)
        {
            StaggerDirection staggerLeft = FindStaggerLeft(new Location(staggerInfo.DescentNodeX, staggerInfo.DescentNodeY), staggerInfo.MembersCount);
            StaggerDirection staggerDown = FindStaggerDown(new Location(staggerInfo.DescentNodeX, staggerInfo.DescentNodeY), staggerInfo.MembersCount);
            StaggerDirection staggerRight = FindStaggerRight(new Location(staggerInfo.DescentNodeX, staggerInfo.DescentNodeY), staggerInfo.MembersCount);

            if (staggerLeft == null && staggerRight == null && staggerDown == null)
            {
                StaggerInfo jumpStaggerInfo = new StaggerInfo(staggerInfo, true);
                _staggers.Add(jumpStaggerInfo);
                _cells[staggerInfo.DescentNodeY][staggerInfo.DescentNodeX] = new FamilyTableCell(staggerInfo.DescentNodeX,
                    staggerInfo.DescentNodeY, new StaggerPlaceholder(jumpStaggerInfo), staggerInfo.Marriage);
                //(_cells[staggerInfo.DescentNodeY][staggerInfo.DescentNodeX] as StaggerPlaceholder).IsJump = true;
                return;
            }

            StaggerDirection destination = staggerDown ?? staggerLeft ?? staggerRight;

            StaggerDirection currentDirection = destination;
            Location previousLocation = new Location(staggerInfo.DescentNodeX, staggerInfo.DescentNodeY);
            int currentX = staggerInfo.DescentNodeX;
            int currentY = staggerInfo.DescentNodeY;

            FamilyTableCell initialPlaceholder = _cells[currentY][currentX];
            bool lastWasHorizontal = false;
            bool lastHorizontalWentLeft = false;

            while (currentDirection != null)
            {
                while (currentDirection.Destination.Y >= _cells.Count)
                {
                    AddRow();
                }
                int stepX = MathExtensions.ClampUnitVector(currentDirection.Destination.X - currentX);
                int stepY = MathExtensions.ClampUnitVector(currentDirection.Destination.Y - currentY);
                bool firstStepTaken = false;
                while (currentX != currentDirection.Destination.X || currentY != currentDirection.Destination.Y)
                {
                    _cells[currentY][currentX] = new FamilyTableCell(currentX, currentY,
                        new Line((stepY != 0 && (!lastWasHorizontal || firstStepTaken) || stepY == 0 && !firstStepTaken && currentDirection != destination),
                        (stepY != 0), (stepX != 0 && firstStepTaken ||
                        !firstStepTaken && stepX < 0 || stepX == 0 && !firstStepTaken && !lastHorizontalWentLeft && lastWasHorizontal),
                        (stepX != 0 && firstStepTaken || !firstStepTaken && stepX > 0 ||
                        stepX == 0 && !firstStepTaken && lastHorizontalWentLeft && lastWasHorizontal)), staggerInfo.Marriage);
                    //(_cells[currentY][currentX] as Line).DEBUG_DATA = string.Concat("StepX: ", stepX, "<br />StepY: ", stepY, "<br />FirstStepTaken:",
                    //    firstStepTaken, "<br />LastWasHorizontal: ", lastWasHorizontal, "<br />LastWasLeft: ", lastHorizontalWentLeft);
                    currentX += stepX;
                    currentY += stepY;
                    if (!firstStepTaken)
                    {
                        firstStepTaken = true;
                    }
                }

                if (currentDirection.NextStep == null)
                {
                    break;
                }

                lastWasHorizontal = (stepX != 0);
                lastHorizontalWentLeft = (stepX < 0);
                previousLocation = currentDirection.Destination;
                currentDirection = currentDirection.NextStep;
            }


            _cells[currentDirection.Destination.Y - 1][currentDirection.Destination.X] = new FamilyTableCell(currentDirection.Destination.X,
                currentDirection.Destination.Y - 1, new Line(true, true, true, true), staggerInfo.Marriage);
            _cells[staggerInfo.DescentNodeY][staggerInfo.DescentNodeX] = initialPlaceholder;

            RegionSpan regionSpan = GetRegionSpan(currentDirection.Destination.X, currentDirection.Destination.Y);
            PlacePersonsResults results = PlacePersons(staggerInfo.Marriage, staggerInfo.Members, staggerInfo.NumberChildren, currentDirection.Destination,
                regionSpan, 0, false);
                //regionSpan.AvailableLeft - (regionSpan.TouchesLeftWall ? 0 : 1), regionSpan.AvailableRight - (regionSpan.TouchesRightWall ? 0 : 1),
                //0, false);

            if (results == null)
            {

                StaggerInfo jumpStaggerInfo = new StaggerInfo(staggerInfo, true);
                _staggers.Add(jumpStaggerInfo);
                _cells[staggerInfo.DescentNodeY][staggerInfo.DescentNodeX] = new FamilyTableCell(staggerInfo.DescentNodeX,
                    staggerInfo.DescentNodeY, new StaggerPlaceholder(jumpStaggerInfo), staggerInfo.Marriage);
                return;
            }

            if (results != null)
            {
                int differenceY = (currentDirection.Destination.Y - previousLocation.Y);
                _cells[currentDirection.Destination.Y - 1][currentDirection.Destination.X] = new FamilyTableCell(currentDirection.Destination.X,
                    currentDirection.Destination.Y - 1, new Line((differenceY > 1), (results.HasMiddle &&
                    (results.MiddleIndex >= 0 && staggerInfo.Members[results.MiddleIndex].IsChild)),
                    (results.ChildrenLeft > 0 || differenceY == 1), (results.ChildrenRight > 0 || differenceY == 1)), staggerInfo.Marriage);

                if (results.MinLineX > currentDirection.Destination.X)
                {
                    for (int x = currentDirection.Destination.X + 1; x < results.MinLineX; x++)
                    {
                        _cells[currentDirection.Destination.Y - 1][x] = new FamilyTableCell(x, currentDirection.Destination.Y - 1,
                            new Line(false, false, true, true), staggerInfo.Marriage);
                    }
                }
                else if (results.MaxLineX < currentDirection.Destination.X)
                {
                    for (int x = currentDirection.Destination.X - 1; x > results.MaxLineX; x--)
                    {
                        _cells[currentDirection.Destination.Y - 1][x] = new FamilyTableCell(x, currentDirection.Destination.Y - 1,
                            new Line(false, false, true, true), staggerInfo.Marriage);
                    }
                }
            }

            /*if (staggerInfo.UniqueID == 7)
            {
                _cells[currentDirection.Destination.Y][currentDirection.Destination.X] = "7 DEST MIDDLE IND:" + results.MiddleIndex + "<br />" +
                    results.MinLineX + "-" + results.MaxLineX;
            }*/

            foreach (FamilyTableMember member in staggerInfo.Members)
            {
                if (!member.IsChild)
                {
                    continue;
                }

                Child memberChild = (Child)member.MemberData;

                foreach (Marriage marriage in memberChild.Marriages)
                {
                    if (!marriage.HaveChildren || marriage.Children == null)
                    {
                        continue;
                    }

                    Location parentLocation = GetLocation(marriage.GetPartner(memberChild.Person));
                    if (memberChild.Marriages.Length == 1)
                    {
                        Location memberChildLocation = GetLocation(memberChild.Person);
                        parentLocation = new Location((int)Math.Round((parentLocation.X + memberChildLocation.X) / 2D), parentLocation.Y);
                    }

                    while (parentLocation.Y + 3 >= _cells.Count)
                    {
                        AddRow();
                    }
                    _marriagesToPlace.Add(new DelayedMarriage(parentLocation, marriage));
                    _cells[parentLocation.Y + 3][parentLocation.X] = new FamilyTableCell(parentLocation.X, parentLocation.Y + 3,
                        new MarriagePlaceholder(marriage), staggerInfo.Marriage);
                    //ExpandMarriage(parentLocation, marriage);
                }
            }

            /*foreach (FamilyTableMember member in staggerInfo.Members)
            {
                if (!member.IsChild)
                {
                    continue;
                }

                foreach (Marriage marriage in (member.MemberData as Child).Marriages)
                {
                    if (!marriage.HaveChildren || marriage.Children == null)
                    {
                        continue;
                    }

                    for (int index = 0; index < marriage.Children.Count; index++)
                    {
                        ExpandPerson(marriage.Children[index]);
                    }
                }
            }*/

            //_cells[currentDirection.Destination.Y][currentDirection.Destination.X] = "STAGGER DESTINATION! (STAGGER #" + staggerInfo.UniqueID + ")";
        }
        private StaggerDirection FindStaggerLeft(Location startLocation, int numberMembers)
        {
            int x = startLocation.X - 1;
            int y = startLocation.Y + 2;

            if (y >= _cells.Count)
            {
                return null;
            }

            while (x >= 0 && _cells[y][x] == null)
            {
                StaggerDirection downStagger = FindStaggerDown(new Location(x, y), numberMembers);
                if (downStagger != null)
                {
                    return new StaggerDirection(new Location(startLocation.X, y), new StaggerDirection(new Location(x, y), downStagger));
                }
                x--;
            }

            return null;
        }
        private StaggerDirection FindStaggerRight(Location startLocation, int numberMembers)
        {
            int x = startLocation.X + 1;
            int y = startLocation.Y + 2;

            if (y >= _cells.Count)
            {
                return null;
            }

            while (x >= 0 && _cells[y][x] == null)
            {
                StaggerDirection downStagger = FindStaggerDown(new Location(x, y), numberMembers);
                if (downStagger != null)
                {
                    return new StaggerDirection(new Location(startLocation.X, y), new StaggerDirection(new Location(x, y), downStagger));
                }
                x++;
            }

            return null;
        }
        private StaggerDirection FindStaggerDown(Location startLocation, int numberMembers) //, int numberChildren)
        {
            int y = startLocation.Y + 1;
            while (y < _cells.Count && _cells[y][startLocation.X] == null)
            {
                if (y % 3 == 0)
                {
                    if (IsRegionEmpty(y, startLocation.X, numberMembers * 2))// - number))
                    {
                        return new StaggerDirection(new Location(startLocation.X, y), null);
                    }
                }
                y++;
            }

            if (y == _cells.Count)
            {
                return new StaggerDirection(new Location(startLocation.X, (int)(3 * Math.Ceiling(y / 3d))), null);
            }

            return null;
        }

        private RegionSpan GetRegionSpan(int x, int y)
        {
            if (_cells[y][x] != null)
            {
                return new RegionSpan(x, y, 0, 0, 0, false, false);
            }

            int availableLeft = 0;
            bool touchesLeft = false;
            for (int left = x - 1; left >= 0; left--)
            {
                if (_cells[y][left] != null)
                {
                    break;
                }
                availableLeft++;

                if (left == 0)
                {
                    touchesLeft = true;
                }
            }

            int availableRight = 0;
            bool touchesRight = false;
            for (int right = x + 1; right < _maxWidth; right++)
            {
                if (_cells[y][right] != null)
                {
                    break;
                }
                availableRight++;

                if (right == _maxWidth - 1)
                {
                    touchesRight = true;
                }
            }

            return new RegionSpan(x, y, 1 + availableLeft + availableRight, availableLeft, availableRight, touchesLeft, touchesRight);
        }
        private bool IsRegionEmpty(int y, int centerX, int width)
        {
            RegionSpan span = GetRegionSpan(centerX, y);

            if (span.TotalWidth - (span.TouchesLeftWall ? 0 : 1) - (span.TouchesRightWall ? 0 : 1) > width)
            {
                return true;
            }

            return false;
        }

        private void RemoveMarriage(Marriage marriage)
        {
            for (int y = 0; y < _cells.Count; y++)
            {
                for (int x = 0; x < _maxWidth; x++)
                {
                    if (_cells[y][x] != null && marriage.Equals(_cells[y][x].ParentMarriage))
                    {
                        _cells[y][x] = null;
                    }
                }
            }
        }
        private List<FamilyTableMember> GetFamilyTableMembers(Marriage marriage)
        {
            List<FamilyTableMember> members = new List<FamilyTableMember>();
            for (int index = 0; index < marriage.Children.Count; index++)
            {
                Child child = marriage.Children[index];
                int marriageMidwayIndex = (int)Math.Round(child.Marriages.Length / 2D);
                if (child.Marriages.Length > 0)
                {
                    for (int marriageIndex = 0; marriageIndex < marriageMidwayIndex; marriageIndex++)
                    {
                        members.Add(new FamilyTableMember(false, child.Marriages[marriageIndex].GetPartner(child.Person), true,
                            (child.Marriages.Length == 1), child.Marriages[marriageIndex].HaveChildren));
                    }
                }
                members.Add(new FamilyTableMember(true, child, false, false, false));
                if (child.Marriages.Length > 0)
                {
                    for (int marriageIndex = marriageMidwayIndex; marriageIndex < child.Marriages.Length; marriageIndex++)
                    {
                        members.Add(new FamilyTableMember(false, child.Marriages[marriageIndex].GetPartner(child.Person), false,
                            (child.Marriages.Length == 1), child.Marriages[marriageIndex].HaveChildren));
                    }
                }
            }
            return members;
        }

        public string Export()
        {
            FileStream file = new FileStream(Path.Combine("rendered", string.Concat(_focus.Person.Handle, "_", _maxGenerations, "gen",
                (_maxGenerations != 1 ? "s" : ""), ".html")), FileMode.Create);
            StreamWriter writer = new StreamWriter(file);

            writer.WriteLine(@"<style>
td
{
  font-size:70%;
}
td.line
{
  height:40px;
  min-height:40px;
  min-width:40px;
  position:relative;
}
td.line img
{
  width:100%;
  height:100%;
}
td.person
{
  border:2px solid black;
}
td.person.focus
{
  background-color:skyblue;
}
</style>
<script>
function showMarriages()
{
    var marriageHandle = document.getElementById('marriage-find').value;
    var marriageCells = document.getElementsByClassName(marriageHandle + '-MARRIAGE');
    for (var index = 0; index < marriageCells.length; index++)
    {
        marriageCells[index].style.backgroundColor = 'pink';
    }
}
</script>");

            writer.WriteLine("<table>");
            foreach (FamilyTableCell[] row in _cells)
            {
                writer.WriteLine("  <tr>");
                foreach (FamilyTableCell cell in row)
                {
                    if (cell == null)
                    {
                        writer.WriteLine("    <td></td>");
                        continue;
                    }

                    if (cell.Occupant is Line)
                    {
                        Line line = (Line)cell.Occupant;
                        writer.Write("    <td class=\"line " + (cell.ParentMarriage != null ? cell.ParentMarriage.Handle : "NULL") + "-MARRIAGE\"><img src=\"");
                        writer.Write(GetLineImage(line));
                        writer.Write("\" title=\"" + line.ToString() + "\" />");
                        if (line.DEBUG_DATA != null)
                        {
                            writer.Write(line.DEBUG_DATA);
                        }
                        writer.WriteLine("</td>");
                    }
                    else if (cell.Occupant is Person || cell.Occupant is Child)
                    {
                        Person person = (cell.Occupant is Child ? (cell.Occupant as Child).Person : (Person)cell.Occupant);
                        writer.Write("    <td class=\"person " + (cell.ParentMarriage != null ? cell.ParentMarriage.Handle : "NULL") + "-MARRIAGE");
                        if (_focus.Person.Equals(person))
                        {
                            writer.Write(" focus");
                        }
                        writer.Write("\">");
                        if (cell.Occupant is Child)
                        {
                            writer.Write("[");
                            writer.Write((cell.Occupant as Child).Generation);
                            writer.Write("]");
                        }
                        writer.Write("(");
                        writer.Write(person.DEBUG_DATA);
                        writer.Write(") ");
                        writer.Write("<a href=\"http://genealogy.obdurodon.org/findPerson.php?person=");
                        writer.Write(person.Handle);
                        writer.Write("\" target=\"_blank\">");
                        writer.Write(person);
                        writer.Write("</a>");
                        writer.WriteLine("</td>");
                    }
                    else if (cell.Occupant is StaggerPlaceholder)
                    {
                        StaggerPlaceholder placeholder = cell.Occupant as StaggerPlaceholder;
                        writer.Write("    <td class=\"" + (cell.ParentMarriage != null ? cell.ParentMarriage.Handle : "NULL") + "-MARRIAGE\">Stagger Placeholder: ");
                        writer.Write(placeholder.Info.UniqueID);
                        writer.Write(" (");
                        if (placeholder.Info.Jumps)
                        {
                            writer.Write("Jump");
                        }
                        else
                        {
                            writer.Write("NoJump");
                        }
                        writer.Write(", Children: ");
                        writer.Write(placeholder.Info.MembersCount);
                        writer.Write(")</td>");
                    }
                    else if (cell.Occupant is string)
                    {
                        writer.Write("    <td class=\"" + (cell.ParentMarriage != null ? cell.ParentMarriage.Handle : "NULL") + "-MARRIAGE\">");
                        writer.Write(cell.Occupant);
                        writer.WriteLine("</td>");
                    }
                    else
                    {
                        writer.WriteLine("    <td></td>");
                    }
                }
                writer.WriteLine("</tr>");
            }
            writer.WriteLine("</table>");

            writer.WriteLine("<input type=\"text\" id=\"marriage-find\" /> <input type=\"button\" onclick=\"showMarriages();\" value=\"Highlight\" />");

            writer.Flush();
            file.Flush();
            writer.Close();

            return file.Name;
        }

        private string GetLineImage(Line line)
        {
            byte directionFlags = 0; // 0000 [up][down][left][right]
            if (line.Up)
            {
                directionFlags += 8;
            }
            if (line.Down)
            {
                directionFlags += 4;
            }
            if (line.Left)
            {
                directionFlags += 2;
            }
            if (line.Right)
            {
                directionFlags += 1;
            }

            switch (directionFlags)
            {
                case 15:
                    {
                        return "images/cross.png";
                    }
                case 14:
                    {
                        return "images/vertical-left.png";
                    }
                case 13:
                    {
                        return "images/vertical-right.png";
                    }
                case 12:
                    {
                        return "images/vertical.png";
                    }
                case 11:
                    {
                        return "images/up-left-right.png";
                    }
                case 10:
                    {
                        return "images/up-left.png";
                    }
                case 9:
                    {
                        return "images/up-right.png";
                    }
                case 7:
                    {
                        return "images/horizontal-down.png";
                    }
                case 6:
                    {
                        return "images/left-down.png";
                    }
                case 5:
                    {
                        return "images/right-down.png";
                    }
                case 3:
                    {
                        return "images/horizontal.png";
                    }
                case 2:
                    {
                        return "images/left.png";
                    }
                default:
                    {
                        return "";
                    }
            }
        }
    }
}
