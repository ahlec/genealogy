﻿using System;

namespace Genealogy.Table
{
    public class StaggerPlaceholder
    {
        public StaggerPlaceholder(StaggerInfo info)
        {
            Info = info;
        }

        public readonly StaggerInfo Info;
        public bool IsJump = false;
    }
}
