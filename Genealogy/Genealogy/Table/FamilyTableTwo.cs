﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Genealogy.Table
{
    public class FamilyTableTwo
    {
        private class TablePerson
        {
            private TablePerson(Person person, Marriage[] marriages, Children children, Children siblings, TablePerson responsibleParent,
                RelationType relation)
            {
                Person = person;
                Marriages = marriages;
                Children = children;
                Siblings = siblings;
                ResponsibleParent = responsibleParent;
                RelationToResponsibleParent = relation;
            }
            public TablePerson(Child child, TablePerson responsible, RelationType relation, Children siblings)
            {
                Person = child.Person;
                ResponsibleParent = responsible;
                Marriages = child.Marriages;
                RelationToResponsibleParent = relation;
                Siblings = siblings;
            }
            public TablePerson(Person person, Children children, TablePerson responsible, RelationType relation)
            {
                Person = person;
                if (children != null && children.Count > 0)
                {
                    Children = children;
                }
                ResponsibleParent = responsible;
                RelationToResponsibleParent = relation;
            }
            public readonly Person Person;
            public readonly Marriage[] Marriages;
            public readonly Children Children;
            public readonly Children Siblings;
            public readonly TablePerson ResponsibleParent;
            public readonly RelationType RelationToResponsibleParent;
            public bool LocationsExplored = false;

            public TablePerson Recreate()
            {
                return new TablePerson(Person, Marriages, Children, Siblings, ResponsibleParent, RelationToResponsibleParent);
            }
        }
        private class Location
        {
            public Location(int x, int y)
            {
                X = x;
                Y = y;
            }

            public readonly int X;
            public readonly int Y;
        }
        private class Placement
        {
            public Placement(TablePerson person, Location location, Direction direction, bool canGoLeftRight)
            {
                Person = person;
                Location = location;
                Direction = direction;
                CanGoLeftRight = canGoLeftRight;
            }
            public readonly TablePerson Person;
            public readonly Location Location;
            public readonly Direction Direction;
            public readonly bool CanGoLeftRight;
        }
        private enum Direction
        {
            Left,
            Right,
            Down
        }
        private enum RelationType
        {
            Spouse,
            Child,
            Sibling
        }
        private class Line
        {
            public Line(bool up, bool down, bool left, bool right, TablePerson responsibleParent)
            {
                Up = up;
                Down = down;
                Left = left;
                Right = right;
                ResponsibleParent = responsibleParent;
            }
            public readonly bool Up;
            public readonly bool Down;
            public readonly bool Left;
            public readonly bool Right;
            public readonly TablePerson ResponsibleParent;

            public override string ToString()
            {
                return string.Join(" ", (Up ? "UP" : null), (Down ? "DOWN" : null), (Right ? "RIGHT" : null), (Left ? "LEFT" : null));
            }
        }

        private class Place
        {
            public Place(int status, Person person)
            {
                Status = status;
                Person = person;
            }
            public int Status;
            public Person Person;
        }

        private Stack<TablePerson> _stack = new Stack<TablePerson>();
        private Stack<TablePerson> _stackBuffer = new Stack<TablePerson>();
        private Stack<Placement> _placements = new Stack<Placement>();
        private Stack<Placement> _placementsBuffer = new Stack<Placement>();
        private Dictionary<TablePerson, Location> _locations = new Dictionary<TablePerson, Location>();
        private Dictionary<Line, Location> _lines = new Dictionary<Line, Location>();
        private Queue<TablePerson> _removeQueue = new Queue<TablePerson>();
        private object[,] _cells = new object[100, 200];
        private Queue<Place> _lastPlacements = new Queue<Place>();

        public FamilyTableTwo(Child focus)
        {
            Focus = focus;
            TablePerson currentPerson = new TablePerson(focus, null, RelationType.Child, null);
            currentPerson.LocationsExplored = true;
            _stack.Push(currentPerson);
            _placements.Push(new Placement(currentPerson, new Location(_cells.GetLength(1) / 2, -1), Direction.Down, false));

            currentPerson = null;
            // begin "func"
            while (_stack.Count > 0)
            {
                currentPerson = _stack.Pop();
                Console.WriteLine("Current person: {0}", currentPerson.Person);
                Console.WriteLine("Stack count: {0}", _stack.Count);
                Console.WriteLine("Placements count: {0}", _placements.Count);
                Console.WriteLine("Placed persons: {0}", _locations.Count);
                Console.SetCursorPosition(0, Console.CursorTop - 4);

                if (_lastPlacements.Count == 10)
                {
                    _lastPlacements.Dequeue();
                }

                while (_placements.Count > 0 && !_placements.Peek().Person.Equals(currentPerson) &&
                    !IsResponsible(currentPerson, _placements.Peek().Person))
                {
                    _placements.Pop();
                }

                if (!currentPerson.LocationsExplored)
                {
                    Location parentLocation = _locations[currentPerson.ResponsibleParent];
                    if (currentPerson.RelationToResponsibleParent == RelationType.Child)
                    {
                        _placements.Push(new Placement(currentPerson, new Location(parentLocation.X + 1, parentLocation.Y + 2), Direction.Right, true));
                        _placements.Push(new Placement(currentPerson, new Location(parentLocation.X, parentLocation.Y + 2), Direction.Left, true));
                    }
                    else if (currentPerson.RelationToResponsibleParent == RelationType.Spouse)
                    {
                        _placements.Push(new Placement(currentPerson, new Location(parentLocation.X + 2, parentLocation.Y), Direction.Right, false));
                        _placements.Push(new Placement(currentPerson, new Location(parentLocation.X - 2, parentLocation.Y), Direction.Left, false));
                    }
                    else if (currentPerson.RelationToResponsibleParent == RelationType.Sibling)
                    {
                        _placements.Push(new Placement(currentPerson, new Location(parentLocation.X + 2, parentLocation.Y - 1), Direction.Right, false));
                        _placements.Push(new Placement(currentPerson, new Location(parentLocation.X - 2, parentLocation.Y - 1), Direction.Left, false));
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                    currentPerson.LocationsExplored = true;
                }

                _lastPlacements.Enqueue(new Place(PlacePerson(currentPerson), currentPerson.Person));
            }
            // end "func"
        }

        public readonly Child Focus;

        private int PlacePerson(TablePerson person)
        {
            if (_locations.ContainsKey(person))
            {
                return 1;
            }

            if (!_placements.Peek().Person.Equals(person))
            {
                RemoveSubtree(person.ResponsibleParent);
                while (IsResponsible(_placements.Peek().Person, person.ResponsibleParent))//_placements.Peek().Person.Equals(person.ResponsibleParent))
                {
                    _placements.Pop();
                }
                _stack.Push(person.ResponsibleParent);//.Recreate());
                return 2;
            }

            Placement currentPlacement = _placements.Pop();
            bool advancePlacement = false;

            if (currentPlacement.Location.Y >= 0 && (currentPlacement.Location.X < 0 || currentPlacement.Location.X >= _cells.GetLength(1) ||
                _cells[currentPlacement.Location.Y, currentPlacement.Location.X] != null)) // If something is in our line row, we can't go further
            {
                if (person.RelationToResponsibleParent == RelationType.Spouse &&
                    _cells[currentPlacement.Location.Y, currentPlacement.Location.X] is TablePerson &&
                    ((_cells[currentPlacement.Location.Y, currentPlacement.Location.X] as TablePerson).Equals(person.ResponsibleParent) ||
                    (_cells[currentPlacement.Location.Y, currentPlacement.Location.X] as TablePerson).ResponsibleParent.Equals(person.ResponsibleParent)))
                {
                    advancePlacement = true;
                }
                else
                {
                    _stack.Push(person);
                    return 3;
                }
            }

            if (advancePlacement || person.RelationToResponsibleParent != RelationType.Spouse && 
                currentPlacement.Location.Y >= 0 && _cells[currentPlacement.Location.Y + 1, currentPlacement.Location.X] != null)
            {
                /*if (currentPlacement.Direction == Direction.Down)
                {
                    _placements.Push(new Placement(person, new Location(currentPlacement.Location.X, currentPlacement.Location.Y + 3),
                        Direction.Down, false));
                    _stack.Push(person);
                    return;
                }*/

                _placementsBuffer.Clear();
                while (_placements.Count > 0 && _placements.Peek().Person.Person.Equals(person.Person))
                {
                    _placementsBuffer.Push(_placements.Pop());
                }
                Location continuedLocation;
                switch (currentPlacement.Direction)
                {
                    case Direction.Left:
                        {
                            continuedLocation = new Location(currentPlacement.Location.X -
                                (person.RelationToResponsibleParent != RelationType.Child ? 2 : 1), currentPlacement.Location.Y);
                            break;
                        }
                    case Direction.Right:
                        {
                            continuedLocation = new Location(currentPlacement.Location.X +
                                (person.RelationToResponsibleParent != RelationType.Child ? 2 : 1), currentPlacement.Location.Y);
                            break;
                        }
                    default:
                        {
                            throw new NotImplementedException();
                        }
                }
                Placement continuedPlacement = new Placement(person, continuedLocation, currentPlacement.Direction, currentPlacement.CanGoLeftRight);
                _placements.Push(continuedPlacement);
                while (_placementsBuffer.Count > 0)
                {
                    _placements.Push(_placementsBuffer.Pop());
                }

                /*if (currentPlacement.CanGoLeftRight)
                {
                    _placements.Push(new Placement(person, new Location(currentPlacement.Location.X, currentPlacement.Location.Y + 3),
                        Direction.Down, false));
                }*/

                _stack.Push(person);
                return 4;
            }

            _cells[currentPlacement.Location.Y + (person.RelationToResponsibleParent != RelationType.Spouse ? 1 : 0),
                currentPlacement.Location.X] = person;
            if (person.RelationToResponsibleParent != RelationType.Spouse && person.ResponsibleParent != null)
            {
                PlaceLine(new Line(false, false, true, true, person.ResponsibleParent), currentPlacement.Location);
                PlaceLine(new Line(false, false, true, true, person.ResponsibleParent), new Location(currentPlacement.Location.X +
                    (currentPlacement.Direction == Direction.Left ? 1 : -1), currentPlacement.Location.Y));
            }
            if (person.RelationToResponsibleParent == RelationType.Spouse)
            {
                PlaceLine(new Line(false, false, true, true, person.ResponsibleParent), new Location(currentPlacement.Location.X +
                    (currentPlacement.Direction == Direction.Left ? 1 : -1), currentPlacement.Location.Y));
            }
            _locations.Add(person, new Location(currentPlacement.Location.X, currentPlacement.Location.Y +
                (person.RelationToResponsibleParent != RelationType.Spouse ? 1 : 0)));

            if (person.Siblings != null)
            {
                int middle = person.Siblings.Count / 2;
                for (int index = middle + 1; index < person.Siblings.Count; index++)
                {
                    _stack.Push(new TablePerson(person.Siblings[index], person, RelationType.Sibling, null));
                }
                for (int index = middle - 1; index > 0; index--)
                {
                    _stack.Push(new TablePerson(person.Siblings[index], person, RelationType.Sibling, null));
                }
            }

            if (person.Marriages != null)
            {
                foreach (Marriage marriage in person.Marriages)
                {
                    _stack.Push(new TablePerson(marriage.GetPartner(person.Person), marriage.Children, person, RelationType.Spouse));
                }
            }
            else if (person.Children != null && person.RelationToResponsibleParent == RelationType.Spouse)
            {
                _stack.Push(new TablePerson(person.Children[person.Children.Count / 2], person, RelationType.Child, person.Children));
            }

            return 0;
        }

        private void PlaceLine(Line line, Location location)
        {
            _cells[location.Y, location.X] = line;
            _lines.Add(line, location);
        }

        private void RemoveSubtree(TablePerson person)
        {
            _removeQueue.Clear();
            _removeQueue.Enqueue(person);

            TablePerson current;
            IEnumerable<TablePerson> toRemove;
            Line[] linesToRemove;
            while (_removeQueue.Count > 0)
            {
                current = _removeQueue.Dequeue();
                if (!_locations.ContainsKey(current))
                {
                    continue;
                }

                _cells[_locations[current].Y, _locations[current].X] = null;
                _locations.Remove(current);
                RemoveFromStack(current);

                toRemove = System.Linq.Enumerable.Where(_locations.Keys, key => key.ResponsibleParent != null &&
                    key.ResponsibleParent.Person.Equals(person.Person));
                linesToRemove = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Where(_lines.Keys, key => key.ResponsibleParent != null &&
                    key.ResponsibleParent.Person.Equals(person.Person)));
                foreach (TablePerson removing in toRemove)
                {
                    _removeQueue.Enqueue(removing);
                }
                foreach (Line line in linesToRemove)
                {
                    _cells[_lines[line].Y, _lines[line].X] = null;
                    _lines.Remove(line);
                }
            }
        }

        private void RemoveFromStack(TablePerson person)
        {
            if (!_stack.Contains(person))
            {
                return;
            }

            _stackBuffer.Clear();
            while (!_stack.Peek().Equals(person))
            {
                _stackBuffer.Push(_stack.Pop());
            }

            _stack.Pop();

            while (_stackBuffer.Count > 0)
            {
                _stack.Push(_stackBuffer.Pop());
            }
        }

        private bool IsResponsible(TablePerson person, TablePerson end)
        {
            return IsResponsible_Recursive(person, end, true);
        }
        private bool IsResponsible_Recursive(TablePerson person, TablePerson end, bool isFirstCheck)
        {
            if (person.ResponsibleParent == null)
            {
                return false;
            }
            if (person.ResponsibleParent.Equals(end))
            {
                return !isFirstCheck;
            }

            return IsResponsible_Recursive(person.ResponsibleParent, end, false);
        }

        public string Export(string filename)
        {
            FileStream file = new FileStream(filename, FileMode.Create);
            StreamWriter writer = new StreamWriter(file);

            writer.WriteLine(@"<style>
td
{
  font-size:70%;
}
td.line
{
  height:40px;
  min-height:40px;
  min-width:40px;
  position:relative;
}
td.line img
{
  width:100%;
  height:100%;
}
td.person
{
  border:2px solid black;
}
td.person.focus
{
  background-color:skyblue;
}
</style>
<script>
function showMarriages()
{
    var marriageHandle = document.getElementById('marriage-find').value;
    var marriageCells = document.getElementsByClassName(marriageHandle + '-MARRIAGE');
    for (var index = 0; index < marriageCells.length; index++)
    {
        marriageCells[index].style.backgroundColor = 'pink';
    }
}
</script>");

            writer.WriteLine("<table>");

            int y;
            int x;
            object cell;
            TablePerson occupant;
            Line line;
            for (y = 0; y < _cells.GetLength(0); y++)
            {
                writer.WriteLine("  <tr>");
                for (x = 0; x < _cells.GetLength(1); x++)
                {
                    cell = _cells[y, x];
                    if (cell is TablePerson)
                    {
                        occupant = cell as TablePerson;
                        writer.Write("    <td class=\"person");
                        if (Focus.Person.Equals(occupant.Person))
                        {
                            writer.Write(" focus");
                        }
                        writer.Write("\">");
                        writer.Write("<a href=\"http://genealogy.obdurodon.org/findPerson.php?person=");
                        writer.Write(occupant.Person.Handle);
                        writer.Write("\" target=\"_blank\">");
                        writer.Write(occupant.Person);
                        writer.Write("</a>");
                        writer.WriteLine("</td>");
                    }
                    else if (cell is Line)
                    {
                        line = cell as Line;
                        writer.Write("    <td class=\"line\"><img src=\"");
                        writer.Write(GetLineImage(line));
                        writer.Write("\" title=\"" + line.ToString() + "\" />");
                        writer.WriteLine("</td>");
                    }
                    else
                    {
                        writer.WriteLine("    <td></td>");
                    }
                }
                writer.WriteLine("</tr>");
            }
            writer.WriteLine("</table>");

            writer.WriteLine("<h4>Last 10 placements:</h4>");
            writer.WriteLine("<ol>");
            foreach (Place place in _lastPlacements)
            {
                writer.WriteLine("<li>{0} (exit code #{1})</li>", place.Person, place.Status);
            }
            writer.WriteLine("</ol>");

            writer.Flush();
            file.Flush();
            writer.Close();

            return file.Name;
        }

        private string GetLineImage(Line line)
        {
            byte directionFlags = 0; // 0000 [up][down][left][right]
            if (line.Up)
            {
                directionFlags += 8;
            }
            if (line.Down)
            {
                directionFlags += 4;
            }
            if (line.Left)
            {
                directionFlags += 2;
            }
            if (line.Right)
            {
                directionFlags += 1;
            }

            switch (directionFlags)
            {
                case 15:
                    {
                        return "images/cross.png";
                    }
                case 14:
                    {
                        return "images/vertical-left.png";
                    }
                case 13:
                    {
                        return "images/vertical-right.png";
                    }
                case 12:
                    {
                        return "images/vertical.png";
                    }
                case 11:
                    {
                        return "images/up-left-right.png";
                    }
                case 10:
                    {
                        return "images/up-left.png";
                    }
                case 9:
                    {
                        return "images/up-right.png";
                    }
                case 7:
                    {
                        return "images/horizontal-down.png";
                    }
                case 6:
                    {
                        return "images/left-down.png";
                    }
                case 5:
                    {
                        return "images/right-down.png";
                    }
                case 3:
                    {
                        return "images/horizontal.png";
                    }
                case 2:
                    {
                        return "images/left.png";
                    }
                default:
                    {
                        return "";
                    }
            }
        }        
    }
}
