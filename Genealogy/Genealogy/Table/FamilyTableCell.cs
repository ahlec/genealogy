﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genealogy.Table
{
    public sealed class FamilyTableCell
    {
        public FamilyTableCell(int x, int y, object data, Marriage parentMarriage)
        {
            X = x;
            Y = y;
            Occupant = data;
            ParentMarriage = parentMarriage;
        }
        public readonly int X;
        public readonly int Y;
        public readonly object Occupant;
        public readonly Marriage ParentMarriage;
    }
}
