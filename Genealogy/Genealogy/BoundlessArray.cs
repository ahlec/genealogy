﻿using System;

namespace Genealogy
{
    public class BoundlessArray<T>
    {
        private class BoundlessItem
        {
        }
        private class BoundlessHub : BoundlessItem
        {
            public BoundlessRow Up; // Hubs aren't adjacent to other Hubs
            public BoundlessRow Down;
            public BoundlessRow Left;
            public BoundlessRow Right;

            public BoundlessHub UpHub;
            public BoundlessHub DownHub;
            public BoundlessHub LeftHub;
            public BoundlessHub RightHub;
        }
        private class BoundlessRow : BoundlessItem
        {
            public BoundlessItem Forward;
            public BoundlessItem Backward;
        }

        private BoundlessHub _hub = new BoundlessHub();
        private const decimal HUB_SEPARATION = 10;

        public T this[int row, int cell]
        {
            get
            {
                int rowHubsToJump = (int)Math.Floor(Math.Abs(row) / HUB_SEPARATION);
                BoundlessHub currentHub = _hub;
                while (rowHubsToJump > 0)
                {
                    if (row < 0)
                    {
                        currentHub = currentHub.UpHub;
                    }
                    else
                    {
                        currentHub = currentHub.DownHub;
                    }

                    if (currentHub == null) // we haven't gotten that far!
                    {
                        return default(T);
                    }
                    else
                    {
                        rowHubsToJump--;
                    }
                }
            }
            set
            {
                asdfgdfgsd
            }
        }
    }
}
