﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Genealogy
{
    public class Xml : XElement
    {
        private Xml(XElement rootElement) : base(rootElement)
        {
        }

        public static Xml Load(string filepath)
        {
            FileStream stream = new FileStream(filepath, FileMode.Open);
            XDocument document = XDocument.Load(stream);
            stream.Close();
            return new Xml(document.Root);
        }

        public object EvaluateXPath(string xpath)
        {
            return this.XPathEvaluate(xpath);
        }

        public XElement SelectElement(string xpath)
        {
            return this.XPathSelectElement(xpath);
        }

        public IEnumerable<XElement> SelectElements(string xpath)
        {
            return this.XPathSelectElements(xpath);
        }
    }
}
