﻿using System;
using System.Collections;
using System.Xml.Linq;

namespace Genealogy
{
    public sealed class Person
    {
        private static Hashtable STORED_PERSONS = new Hashtable();

        private Person(Xml xml, string handle)
        {
            Handle = handle;
            XElement personNode = xml.SelectElement("//persons/person[@id='" + handle + "']");
            XElement nameNode = personNode.Element("name");
            FirstName = nameNode.Element("firstName").Value;
            if (nameNode.Element("patronymic") != null)
            {
                Patronymic = nameNode.Element("patronymic").Value;
            }

            if (personNode.Element("parents") != null)
            {
                XElement fatherNode = personNode.Element("parents").Element("father");
                if (fatherNode != null && fatherNode.Attribute("pointer") != null)
                {
                    Father = Person.Get(xml, fatherNode.Attribute("pointer").Value);
                }
                XElement motherNode = personNode.Element("parents").Element("mother");
                if (motherNode != null && motherNode.Attribute("pointer") != null)
                {
                    Mother = Person.Get(xml, motherNode.Attribute("pointer").Value);
                }
            }

            if (Father != null && Mother != null)
            {
                XElement parentsMarriageNode = xml.SelectElement("//marriages/marriage[@male='" + Father.Handle + "' and @female='" +
                    Mother.Handle + "']");
                if (parentsMarriageNode != null)
                {
                    ParentsMarriageHandle = parentsMarriageNode.Attribute("id").Value;
                }
            }
        }

        public static Person Get(Xml xml, string handle)
        {
            if (string.IsNullOrWhiteSpace(handle))
            {
                throw new ArgumentNullException();
            }

            if (STORED_PERSONS.ContainsKey(handle))
            {
                return STORED_PERSONS[handle] as Person;
            }

            Person newlyLoaded = new Person(xml, handle);
            STORED_PERSONS.Add(handle, newlyLoaded);
            return newlyLoaded;
        }

        public readonly string Handle;
        public readonly string FirstName;
        public readonly string Patronymic;
        public readonly Person Father;
        public readonly Person Mother;
        public readonly string ParentsMarriageHandle;

        public bool IsBloodDescendentOf(Person ancestor)
        {
            if (Equals(ancestor))
            {
                return true;
            }

            if (Father != null && Father.IsBloodDescendentOf(ancestor) || Mother != null && Mother.IsBloodDescendentOf(ancestor))
            {
                return true;
            }

            return false;
        }

        public object DEBUG_DATA;

        public override bool Equals(object obj)
        {
            if (!(obj is Person))
            {
                return false;
            }
            Person other = obj as Person;
            return (string.Equals(Handle, other.Handle));
        }
        public override int GetHashCode()
        {
            return Handle.GetHashCode();
        }
        public override string ToString()
        {
            return string.Join(" ", FirstName, Patronymic, string.Concat("(", Handle, ")"));
        }
    }
}
