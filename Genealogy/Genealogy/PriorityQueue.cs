﻿using System;
using System.Collections.Generic;

namespace Genealogy
{
    public class PriorityQueue<T> where T : IComparable<T>
    {
        private List<T> _array = new List<T>();

        public PriorityQueue()
        {
            Count = 0;
        }

        public int Count { get; private set; }

        public void Add(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            int insertIndex = _array.Count;
            _array.Add(item);
            Count++;

            while (insertIndex > 0)
            {
                int parentIndex = (int)Math.Floor(insertIndex / 2D);
                if (_array[insertIndex].CompareTo(_array[parentIndex]) <= 0)
                {
                    break;
                }

                T tempParent = _array[parentIndex];
                _array[parentIndex] = _array[insertIndex];
                _array[insertIndex] = tempParent;
                insertIndex = parentIndex;
            }
        }
        public T Pop()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException();
            }

            T poppedValue = _array[0];
            Count--;
            if (Count == 0)
            {
                _array.RemoveAt(0);
                return poppedValue;
            }

            _array[0] = _array[_array.Count - 1];
            _array.RemoveAt(_array.Count - 1);

            int currentIndex = 0;
            while (currentIndex < _array.Count)
            {
                int greaterChildIndex = currentIndex * 2 + 1;
                if (greaterChildIndex >= _array.Count)
                {
                    break;
                }

                if (greaterChildIndex + 1 < _array.Count && _array[greaterChildIndex + 1].CompareTo(_array[greaterChildIndex]) >= 0)
                {
                    greaterChildIndex++;
                }

                if (_array[greaterChildIndex].CompareTo(_array[currentIndex]) < 0)
                {
                    break;
                }

                T tempCurrent = _array[currentIndex];
                _array[currentIndex] = _array[greaterChildIndex];
                _array[greaterChildIndex] = tempCurrent;
                currentIndex = greaterChildIndex;
            }

            return poppedValue;
        }
        public void Clear()
        {
            Count = 0;
            _array.Clear();
        }
    }
}
