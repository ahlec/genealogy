﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace Genealogy
{
    public sealed class Children
    {
        private readonly Child[] _children;

        public Children(Xml xml, Marriage marriage, XElement childrenElement, int generationsToAllow, int currentGeneration)
        {
            Marriage = marriage;

#warning "unordered children listing right now"
            Count = childrenElement.Elements().Count();
            _children = new Child[Count];
            int index = 0;
            foreach (XElement child in childrenElement.Elements())
            {
                _children[index] = new Child(xml, child.Attribute("pointer").Value, currentGeneration, generationsToAllow);
                index++;
            }
        }

        public readonly Marriage Marriage;
        public readonly int Count;

        public Child this[int index]
        {
            get
            {
                return _children[index];
            }
        }

        public override string ToString()
        {
            if (Count == 1)
            {
                return "1 child";
            }

            return string.Concat(Count, " children");
        }
    }
}
