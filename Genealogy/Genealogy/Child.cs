﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Genealogy
{
    public sealed class Child : IAncenstor, IComparable<Child>
    {
        public Child(Xml xml, string handle, int currentGeneration, int generationsToAllow)
        {
            Person = Person.Get(xml, handle);
            Generation = currentGeneration;

            List<Marriage> marriages = new List<Marriage>();
            foreach (XElement marriage in xml.SelectElements("//marriages/marriage[@male='" + handle + "' or @female='" + handle + "']"))
            {
                marriages.Add(Marriage.Get(xml, marriage.Attribute("id").Value, generationsToAllow, currentGeneration));
            }
            Marriages = marriages.ToArray();
        }

        public readonly int Generation;
        public readonly Person Person;
        public readonly Marriage[] Marriages;

        public int CompareTo(Child childB)
        {
            return childB.GetChildrenCount().CompareTo(GetChildrenCount());
        }

        public int GetChildrenCount()
        {
            int count = 0;

            foreach (Marriage marriage in Marriages)
            {
                if (marriage.HaveChildren)
                {
                    count += marriage.Children.Count;
                }
            }

            return count;
        }

        public int GetDescendantsCount()
        {
            int count = 0;

            if (Marriages != null)
            {
                for (int index = 0; index < Marriages.Length; index++)
                {
                    count += Marriages[index].GetDescendantsCount();
                }
            }

            return count;
        }
        public int GetSelfAndDescendantsCount()
        {
            int count = 1;

            if (Marriages != null)
            {
                for (int index = 0; index < Marriages.Length; index++)
                {
                    count += Marriages[index].GetSelfAndDescendantsCount() - 1; // Subtract from self this "Child" -- either the Husband or the Wife
                }
            }

            return count;
        }

        public override string ToString()
        {
            return Person.ToString();
        }
    }
}
