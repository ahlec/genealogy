﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genealogy
{
    public static class MathExtensions
    {
        public static int ClampUnitVector(int value)
        {
            if (value > 1)
            {
                return 1;
            }
            if (value < -1)
            {
                return -1;
            }

            return value;
        }
    }
}
