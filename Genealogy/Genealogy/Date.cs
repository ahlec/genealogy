﻿using System;

namespace Genealogy
{
    public class Date
    {
        public readonly bool Known;
        public readonly int Year;
    }
}
